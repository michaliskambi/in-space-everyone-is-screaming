{
  Copyright 2017-2022 Michalis Kamburelis.

  This file is part of "in-space-everyone-is-screaming".

  "in-space-everyone-is-screaming" is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  "in-space-everyone-is-screaming" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with "in-space-everyone-is-screaming"; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA

  ----------------------------------------------------------------------------
}
{ HUD UI. }
unit GameHUD;

interface

uses CastleUIControls, CastleRectangles;

type
  TLifeBar = class(TUIControl)
  public
    procedure Render; override;
  end;

var
  LifeBar: TLifeBar;

implementation

uses CastleGLUtils, CastleColors, CastleVectors, CastleUtils,
  GameEntities;

{ TLifeBar ------------------------------------------------------------ }

procedure TLifeBar.Render;
var
  R, RFull: TFloatRectangle;
begin
  if Player.Life <= 0 then
    GLFadeRectangleDark(ParentRect, Red, 1.0);

  RFull := RenderRect;
  R := RFull.Grow(Round(-2 * UIScale));

  DrawRectangle(RFull, Vector4(1.0, 0.5, 0.5, 0.2));
  if Player.Life > 0 then
  begin
    R.Height := Clamped(
      MapRange(Player.Life, 0, Player.MaxLife, 0, R.Height), 0, R.Height);
    DrawRectangle(R, Vector4(1, 0, 0, 0.9));
  end;
end;

end.
