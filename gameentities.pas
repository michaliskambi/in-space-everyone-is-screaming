{
  Copyright 2017-2022 Michalis Kamburelis.

  This file is part of "in-space-everyone-is-screaming".

  "in-space-everyone-is-screaming" is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  "in-space-everyone-is-screaming" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with "in-space-everyone-is-screaming"; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA

  ----------------------------------------------------------------------------
}

{ Rockets, explosions etc. }
unit GameEntities;

interface

uses SysUtils, Generics.Collections,
  CastleScene, CastleControls, CastleLog, Castle2DSceneManager,
  CastleFilesUtils, CastleSceneCore, CastleKeysMouse, CastleColors,
  CastleUIControls, CastleTransform, CastleVectors, CastleTimeUtils, CastleUtils,
  CastleTriangles, CastleCameras, X3DNodes, X3DLoad, CastleBoxes, CastleFlashEffect;

type
  TExposedScene = class(TCastle2DScene)
  public
    function SimpleSphereCollision(const Pos: TVector3; const Radius: Single): boolean;
  end;

  TEntity = class
  strict private
    FSpawned: boolean;
  protected
    Scene: TExposedScene;
    Transform: TCastleTransform;
    property Spawned: boolean read FSpawned;
    function Visible: boolean;
  strict protected
    procedure Spawn;
    procedure Unspawn;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AfterConstruction; override;
    procedure Update(const SecondsPassed: TFloatTime); virtual;
    function SimpleSphereCollision(const Pos: TVector3; const Radius: Single): boolean;
  end;

  { Entity that may (if @link(Gravity)) be affected by gravity or
    (if @link(Jumping) > 0) by jumping. }
  TGravityEntity = class(TEntity)
  strict private
    const
      HeightEpsilon = 1;
    function GetPosition: TVector3;
    procedure SetPosition(const Value: TVector3);
  strict protected
    Gravity: boolean;
  public
    Jumping: TFloatTime;
    property Position: TVector3 read GetPosition write SetPosition;
    procedure Update(const SecondsPassed: TFloatTime); override;
  end;

  TPlayer = class(TGravityEntity)
  public
    const
      MaxLife = 100;
    var
      MovingLeft: boolean;
      ShootDelay: TFloatTime;
      Life: Single;
    constructor Create;
    procedure Update(const SecondsPassed: TFloatTime); override;
  end;

  TRocket = class(TEntity)
  strict private
    const
      Radius = 1;
      Speed = 200;
      LifeDuration = 10;
    var
      Direction: TVector3;
      LifeTime: TFloatTime;
      Shooter: TEntity;
  public
    constructor Create(const NodeTemplate: TX3DRootNode);
    procedure Update(const SecondsPassed: TFloatTime); override;
    procedure Spawn(const Pos: TVector3; const MovingLeft: boolean; const AShooter: TEntity);
    procedure Unspawn(const Explode: boolean);
  end;

  TRocketList = class(specialize TObjectList<TRocket>)
  strict private
    NodeTemplate: TX3DRootNode;
    function FindUnused: TRocket;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Update(const SecondsPassed: TFloatTime);
    procedure Spawn(const Pos: TVector3; const MovingLeft: boolean; const AShooter: TEntity);
  end;

  TExplosion = class(TEntity)
  strict private
    LifeTime: TFloatTime;
    procedure Unspawn;
  public
    constructor Create(const NodeTemplate: TX3DRootNode);
    procedure Update(const SecondsPassed: TFloatTime); override;
    procedure Spawn(const Pos: TVector3);
  end;

  TExplosionList = class(specialize TObjectList<TExplosion>)
  strict private
    function FindUnused: TExplosion;
  private
    ExplosionLife: TFloatTime;
  public
    constructor Create;
    procedure Update(const SecondsPassed: TFloatTime);
    procedure Spawn(const Pos: TVector3);
  end;

  TEnemy = class(TGravityEntity)
  strict private
    const
      InitialTimeToShoot = 1;
    var
      TimeToShoot: TFloatTime;
      ShootLeft: boolean;
  public
    constructor Create(const NodeTemplate: TX3DRootNode);
    procedure Spawn;
    procedure Unspawn;
    procedure Update(const SecondsPassed: TFloatTime); override;
  end;

  TEnemyList = class(specialize TObjectList<TEnemy>)
  public
    constructor Create;
    function SimpleSphereCollision(const Pos: TVector3; const Radius: Single): TEnemy;
    procedure Update(const SecondsPassed: TFloatTime);
  end;

var
  { Global instances of classes defined in this unit. }

  Map: TExposedScene;
  Player: TPlayer;
  Rockets: TRocketList;
  Explosions: TExplosionList;
  Enemies: TEnemyList;

  { Scene manager is created and destroyed by Game unit,
    but it is exposed to this unit,
    to add/remove items to SceneManager.Items in this unit's implementation. }
  SceneManager: TCastle2DSceneManager;

  DebugNavigation: TCastle2DNavigation;

  { Exposed to query Container.Pressed in Update() }
  Container: TCastleContainer;

  Flash: TCastleFlashEffect;

  { Updated each frame, before calling Update() methods. }
  VisibleArea: TBox3D;

implementation

uses Math;

{ TExposedScene ------------------------------------------------------------------ }

function TExposedScene.SimpleSphereCollision(const Pos: TVector3; const Radius: Single): boolean;
begin
  Result := SphereCollision(Pos, Radius, nil);
end;

{ TEntity -------------------------------------------------------------------- }

constructor TEntity.Create;
begin
  inherited;
  Scene := TExposedScene.Create(nil); // freed by TRocket.Destroy
  // Scene.Spatial := [ssRendering, ssDynamicCollisions]; // not useful

  Transform := TCastleTransform.Create(nil); // freed by TRocket.Destroy
  Transform.Add(Scene);
end;

procedure TEntity.AfterConstruction;
begin
  inherited;
  // PrepareResources after descendant's constructor loaded Scene.RootNode
  SceneManager.PrepareResources(Scene);
end;

destructor TEntity.Destroy;
begin
  FreeAndNil(Scene);
  FreeAndNil(Transform);
  inherited;
end;

procedure TEntity.Spawn;
begin
  SceneManager.Items.Add(Transform);
  FSpawned := true;
  Scene.ResetTimeAtLoad;
end;

procedure TEntity.Unspawn;
begin
  FSpawned := false;
  SceneManager.Items.Remove(Transform);
end;

function TEntity.Visible: boolean;
begin
  Result := VisibleArea.Grow(Scene.BoundingBox.MaxSize).Contains(Transform.Translation);
end;

procedure TEntity.Update(const SecondsPassed: TFloatTime);
begin
end;

function TEntity.SimpleSphereCollision(const Pos: TVector3; const Radius: Single): boolean;
begin
  Result := Scene.SimpleSphereCollision(Pos - Transform.Translation, Radius);
end;

{ TGravityEntity ------------------------------------------------------------- }

function TGravityEntity.GetPosition: TVector3;
begin
  Result := Transform.Translation;
end;

procedure TGravityEntity.SetPosition(const Value: TVector3);
var
  NewBox: TBox3D;
begin
  NewBox := Transform.BoundingBox.Translate(Value - Transform.Translation);
  NewBox.Data[0].Y += HeightEpsilon; // remove bottom part, to not collide with ground too soon
  if not Map.BoxCollision(NewBox, nil) then
    Transform.Translation := Value;
end;

procedure TGravityEntity.Update(const SecondsPassed: TFloatTime);
var
  Pos: TVector3;

  procedure GravityJumpingUpdate;
  const
    JumpSpeed = 100;
    GravitySpeed = 80;
  var
    AboveHeight: Single;
    IsAbove: boolean;
    AboveGround: PTriangle;
    PosTest: TVector3;
    NewBox: TBox3D;
  begin
    Jumping := Max(0, Jumping - SecondsPassed);
    if Jumping <> 0 then
    begin
      { jumping }
      Pos.Y += JumpSpeed * SecondsPassed;

      NewBox := Transform.BoundingBox.Translate(Pos - Transform.Translation);
      if Map.BoxCollision(NewBox, nil) then
      begin
        { abort jump when you bang your head with a ceiling }
        Pos := Transform.Translation;
        Jumping := 0;
      end;
    end else
    if Gravity then
    begin
      { gravity }
      PosTest := Pos;
      PosTest.Y += HeightEpsilon;
      PosTest.Z := 0;
      IsAbove := Scene.Height(PosTest, AboveHeight, AboveGround);
      if IsAbove then
      begin
        if AboveHeight < HeightEpsilon then
          { grow, immediately }
          Pos.Y += HeightEpsilon - AboveHeight
        else
          { fall down }
          Pos.Y -= Min(GravitySpeed * SecondsPassed, AboveHeight - HeightEpsilon);
      end else
        Pos.Y -= GravitySpeed * SecondsPassed;
    end;
  end;

begin
  inherited;
  { Check Spawned, otherwise Scene doesn't exist in the game world,
    so you cannot call Scene.Height.
    Check Visible for optimization: don't do this for invisible enemies. }
  if Spawned and Visible then
  begin
    Pos := Position;
    GravityJumpingUpdate; // modifies Pos
    Position := Pos;
  end;
end;

{ TPlayer -------------------------------------------------------------------- }

constructor TPlayer.Create;
begin
  inherited Create;

  Scene.Load('castle-data:/player.castle-anim-frames');
  Scene.Spatial := [ssDynamicCollisions]; // more precise collisions
  Scene.ProcessEvents := true;
  Scene.PlayAnimation('walk_left', true);
  Scene.TimePlayingSpeed := 2;

  Transform.Translation := Vector3(0, 10, 0);

  Gravity := true;

  Life := MaxLife;

  Spawn;
end;

procedure TPlayer.Update(const SecondsPassed: TFloatTime);
var
  Pos: TVector3;

  procedure ShootingUpdate;
  const
    InitialShootDelay = 0.1;
  begin
    if Container.Pressed[keySpace] then
    begin
      if ShootDelay = 0 then
      begin
        ShootDelay := InitialShootDelay;
        Rockets.Spawn(Pos + Vector3(0, Scene.BoundingBox.Size[1] / 2, 0),
          MovingLeft, Player);
      end;
    end;

    ShootDelay := Max(0, ShootDelay - SecondsPassed);
  end;

  procedure ChangeAnimation(const NewAnimation: string; const Looping: boolean);
  begin
    if (Scene.CurrentAnimation = nil) or
       (Scene.CurrentAnimation.X3DName <> NewAnimation) then
    begin
      if Looping then
        Scene.PlayAnimation(NewAnimation, true)
      else
        Scene.PlayAnimation(NewAnimation, false);
    end;
  end;

const
  PlayerSpeed = 60;
begin
  inherited;

  if Life <= 0 then
  begin
    ChangeAnimation('dying', false);
    Exit;
  end;

  Pos := Position;

  if Container.Pressed[keyArrowLeft] xor Container.Pressed[keyArrowRight] then
  begin
    if Container.Pressed[keyArrowLeft] then
    begin
      Pos.X -= PlayerSpeed * SecondsPassed;
      MovingLeft := true;
    end;

    if Container.Pressed[keyArrowRight] then
    begin
      Pos.X += PlayerSpeed * SecondsPassed;
      MovingLeft := false;
    end;
  end;

  Position := Pos;

  if MovingLeft then
    ChangeAnimation('walk_left', true)
  else
    ChangeAnimation('walk_right', true);

  ShootingUpdate;
end;

{ TRocket -------------------------------------------------------------------- }

constructor TRocket.Create(const NodeTemplate: TX3DRootNode);
begin
  inherited Create;
  Scene.Load(NodeTemplate.DeepCopy as TX3DRootNode, true);
end;

procedure TRocket.Update(const SecondsPassed: TFloatTime);
var
  EnemyCollides: TEnemy;
begin
  inherited;
  if Spawned then
  begin
    LifeTime -= SecondsPassed;
    if (LifeTime <= 0) or (not Visible) then
    begin
      Unspawn(false);
    end else
    begin
      Transform.Translation := Transform.Translation + Direction * SecondsPassed * Speed;

      if Map.SimpleSphereCollision(Transform.Translation, Radius) then
      begin
        Unspawn(true);
      end else
      if (Player <> Shooter) and
         Player.SimpleSphereCollision(Transform.Translation, Radius) then
      begin
        Unspawn(true);
        Player.Life := Player.Life - RandomFloatRange(10, 20);
        Flash.Flash(Red, true);
      end else
      begin
        EnemyCollides := Enemies.SimpleSphereCollision(Transform.Translation, Radius);
        if (EnemyCollides <> nil) and
           // prevent from shooting yourself
           (EnemyCollides <> Shooter) then
        begin
          Unspawn(true);
          EnemyCollides.Unspawn;
        end;
      end;
    end;
  end;
end;

procedure TRocket.Spawn(const Pos: TVector3; const MovingLeft: boolean; const AShooter: TEntity);
begin
  inherited Spawn;
  Transform.Translation := Pos;
  LifeTime := LifeDuration;
  Shooter := AShooter;
  if MovingLeft then
  begin
    Transform.Rotation := Vector4(0, 1, 0, Pi);
    Direction := Vector3(-1, 0, 0)
  end else
  begin
    Transform.Rotation := Vector4(0, 1, 0, 0);
    Direction := Vector3( 1, 0, 0);
  end;
end;

procedure TRocket.Unspawn(const Explode: boolean);
begin
  if Explode then
    Explosions.Spawn(Transform.Translation);
  inherited Unspawn;
end;

{ TRocketList ---------------------------------------------------------------- }

constructor TRocketList.Create;
const
  PoolCount = 50;
var
  I: Integer;
begin
  inherited Create(true);
  NodeTemplate := LoadNode('castle-data:/rocket.x3d');
  for I := 0 to PoolCount - 1 do
    Add(TRocket.Create(NodeTemplate));
end;

destructor TRocketList.Destroy;
begin
  inherited;
  // remove NodeTemplate once all items (they all refer to the same NodeTemplate) are freed
  FreeAndNil(NodeTemplate);
end;

procedure TRocketList.Update(const SecondsPassed: TFloatTime);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[I].Update(SecondsPassed);
end;

function TRocketList.FindUnused: TRocket;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    if not Items[I].Spawned then
      Exit(Items[I]);
  Result := nil;
end;

procedure TRocketList.Spawn(const Pos: TVector3; const MovingLeft: boolean; const AShooter: TEntity);
var
  Rocket: TRocket;
begin
  Rocket := FindUnused;
  if Rocket <> nil then
    Rocket.Spawn(Pos, MovingLeft, AShooter);
end;

{ TExplosion ----------------------------------------------------------------- }

constructor TExplosion.Create(const NodeTemplate: TX3DRootNode);
begin
  inherited Create;
  Scene.Load(NodeTemplate.DeepCopy as TX3DRootNode, true);
  Scene.ProcessEvents := true;
end;

procedure TExplosion.Update(const SecondsPassed: TFloatTime);
begin
  inherited;
  if Spawned then
  begin
    LifeTime -= SecondsPassed;
    if LifeTime <= 0 then
      Unspawn;
  end;
end;

procedure TExplosion.Spawn(const Pos: TVector3);
begin
  inherited Spawn;
  Transform.Translation := Pos;
  LifeTime := Explosions.ExplosionLife;
  Scene.PlayAnimation('Animate', true);
end;

procedure TExplosion.Unspawn;
begin
  inherited Unspawn;
end;

{ TExplosionList ------------------------------------------------------------- }

constructor TExplosionList.Create;
const
  PoolCount = 50;
var
  NodeTemplate: TX3DRootNode;
  I: Integer;
begin
  inherited Create(true);
  NodeTemplate := LoadNode('castle-data:/explode.x3dv');
  try
    for I := 0 to PoolCount - 1 do
      Add(TExplosion.Create(NodeTemplate));
  finally FreeAndNil(NodeTemplate) end;

  ExplosionLife := Items[0].Scene.AnimationDuration('Animate');
  WritelnLog('Explosion life is %f', [ExplosionLife]);
end;

procedure TExplosionList.Update(const SecondsPassed: TFloatTime);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[I].Update(SecondsPassed);
end;

function TExplosionList.FindUnused: TExplosion;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    if not Items[I].Spawned then
      Exit(Items[I]);
  Result := nil;
end;

procedure TExplosionList.Spawn(const Pos: TVector3);
var
  Explosion: TExplosion;
begin
  Explosion := FindUnused;
  if Explosion <> nil then
    Explosion.Spawn(Pos);
end;

{ TEnemy --------------------------------------------------------------------- }

constructor TEnemy.Create(const NodeTemplate: TX3DRootNode);
begin
  inherited Create;
  Scene.Load(NodeTemplate.DeepCopy as TX3DRootNode, true);
  Scene.ProcessEvents := true;

  Gravity := true;

  Spawn;
end;

procedure TEnemy.Spawn;
begin
  inherited;
  TimeToShoot := InitialTimeToShoot;
  ShootLeft := RandomBoolean;

  // initial random position
  repeat
    Transform.Translation := Vector3(
      RandomFloatRange({Map.BoundingBox.Data[0].X} 100, Map.BoundingBox.Data[1].X),
      RandomFloatRange(10, 90),
      0);
  until not Map.BoxCollision(Transform.BoundingBox, nil);

  Scene.PlayAnimation('animation', true);
  Scene.TimePlayingSpeed := RandomFloatRange(0.9, 1.1); // desynchronize animations
end;

procedure TEnemy.Unspawn;
begin
  inherited;
end;

procedure TEnemy.Update(const SecondsPassed: TFloatTime);
begin
  inherited;
  if Spawned and Visible then
  begin
    TimeToShoot -= SecondsPassed;
    if TimeToShoot < 0 then
    begin
      Rockets.Spawn(Transform.Translation + Vector3(0, 6.15 * 2, 0), ShootLeft, Self);
      ShootLeft := not ShootLeft;
      repeat
        TimeToShoot += InitialTimeToShoot;
      until TimeToShoot > 0;
    end;
  end;
end;

{ TEnemyList ----------------------------------------------------------------- }

constructor TEnemyList.Create;
const
  InitialCount = 70;
var
  NodeTemplate: TX3DRootNode;
  I: Integer;
begin
  inherited Create(true);
  NodeTemplate := LoadNode('castle-data:/enemy_robot.x3dv');
  try
    for I := 0 to InitialCount - 1 do
      Add(TEnemy.Create(NodeTemplate));
  finally FreeAndNil(NodeTemplate) end;
end;

function TEnemyList.SimpleSphereCollision(const Pos: TVector3; const Radius: Single): TEnemy;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    if Items[I].Visible { optimization: do not check for collisions with invisible enemies } and
       Items[I].Spawned and
       Items[I].SimpleSphereCollision(Pos, Radius) then
      Exit(Items[I]);
  Result := nil;
end;

procedure TEnemyList.Update(const SecondsPassed: TFloatTime);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    Items[I].Update(SecondsPassed);
end;

finalization
  FreeAndNil(Rockets);
  FreeAndNil(Explosions);
  FreeAndNil(Enemies);
  FreeAndNil(Player);
end.
