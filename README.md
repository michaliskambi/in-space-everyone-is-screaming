# In Space Everyone Is Screaming

Simple platformer-shooter using Castle Game Engine.

Done in roughly 12 hours.

Data created mostly in Blender.
Some X3D files created in a text editor.
Textures downloaded from the Internet.

Code is in modern Object Pascal using Castle Game Engine.
You will need to download Castle Game Engine
( http://castle-engine.sourceforge.io/ ) to compile this project.
You can compile:
- from Lazarus (open in_space_everyone_is_screaming_standalone.lpi)
- or you can compile using the Castle Game Engine build tool:
  https://github.com/castle-engine/castle-engine/wiki/Build-Tool

## License

Feel free to modify and redistribute on terms of
the GNU General Public License, version 2 or later.

Copyright Michalis Kamburelis.
