{
  Copyright 2017-2017, 2022 Michalis Kamburelis.

  This file is part of "in-space-everyone-is-screaming".

  "in-space-everyone-is-screaming" is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  "in-space-everyone-is-screaming" is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with "in-space-everyone-is-screaming"; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA

  ----------------------------------------------------------------------------
}

{ Implements the game logic, independent from mobile / standalone. }
unit Game;

interface

uses CastleWindow;

var
  Window: TCastleWindow;

implementation

uses SysUtils,
  CastleScene, CastleControls, CastleLog, Castle2DSceneManager,
  CastleFilesUtils, CastleSceneCore, CastleKeysMouse, CastleColors,
  CastleUIControls, CastleTransform, CastleVectors, CastleTimeUtils, CastleUtils,
  CastleTriangles, CastleCameras, X3DNodes, X3DLoad, CastleBoxes, CastleFlashEffect,
  CastleMessages,
  GameEntities, GameHUD;

var
  TutorialDone: boolean;

{ routines ------------------------------------------------------------------- }

{ One-time initialization of resources. }
procedure ApplicationInitialize;
var
  SimpleBackground: TCastleSimpleBackground;
begin
  { This line is *possibly* needed (the need not proven by testing)
    because CGE default of TCastleTransform.DefaultOrientation
    is now adjusted to what Blender->glTF exporter does.
    See https://github.com/castle-engine/castle-engine/wiki/Upgrading-to-Castle-Game-Engine-7.0 }
  TCastleTransform.DefaultOrientation := otUpYDirectionMinusZ;

  Container := Window.Container;

  SimpleBackground := TCastleSimpleBackground.Create(Application);
  Window.Controls.InsertBack(SimpleBackground);

  SceneManager := TCastle2DSceneManager.Create(Application);
  SceneManager.FullSize := true;
  SceneManager.Camera.Orthographic.Height := 100;
  SceneManager.Transparent := true;
  Window.Controls.InsertFront(SceneManager);

  DebugNavigation := TCastle2DNavigation.Create(Application);
  DebugNavigation.Exists := false;
  SceneManager.InsertFront(DebugNavigation);

  Map := TExposedScene.Create(Application);
  Map.Load('castle-data:/map.x3dv');
  Map.Spatial := [ssRendering, ssDynamicCollisions];
  Map.ProcessEvents := true;
  Map.PlayAnimation('map_animation', true);
  SceneManager.Items.Add(Map);
  SceneManager.Items.MainScene := Map;

  Flash := TCastleFlashEffect.Create(Application);
  Window.Controls.InsertFront(Flash);

  Player := TPlayer.Create;
  Rockets := TRocketList.Create;
  Explosions := TExplosionList.Create;
  Enemies := TEnemyList.Create;

  VisibleArea := TBox3D.Empty;

  LifeBar := TLifeBar.Create(Application);
  LifeBar.Anchor(hpLeft, 10);
  LifeBar.Anchor(vpBottom, 10);
  LifeBar.Width := 40;
  LifeBar.Height := 100;
  Window.Controls.InsertFront(LifeBar);

  Theme.ImagesPersistent[tiWindow].Url := 'castle-data:/WindowDarkTransparent.png';
end;

procedure WindowRender(Container: TUIContainer);
var
  S, SPos: string;

(* // This test is no longer useful
  procedure TestHeight;
  var
    IsAbove: boolean;
    AboveHeight: Single;
    AboveGround: P3DTriangle;
    PosTest: TVector3;
  begin
    if Map.PointingDeviceOverItem <> nil then
    begin
      PosTest := Map.PointingDeviceOverPoint;
      PosTest[2] := 0;
      IsAbove := SceneManager.Items.WorldHeight(PosTest, AboveHeight, AboveGround);
      if IsAbove then
        S += Format(' Above %f', [AboveHeight])
      else
        S += ' Above Infinite Abyss';
    end else
      S += ' Above Unknown (not picked Map)';
  end;
*)

begin
  SPos := Player.Position.ToString;
  S := Format('FPS: %s, Player position %s', [Container.Fps.ToString, SPos]);
  // TestHeight;
  UIFont.Print(LifeBar.RenderRect.Right + 10, 10, Yellow, S);
end;

procedure WindowUpdate(Container: TUIContainer);
var
  SecondsPassed: Single;
  CameraPos: TVector3;
begin
  SecondsPassed := Container.Fps.SecondsPassed;

  Player.Update(SecondsPassed);

  CameraPos := SceneManager.Camera.Translation;
  CameraPos.X := Player.Position.X - SceneManager.Camera.Orthographic.EffectiveWidth / 2;
  SceneManager.Camera.Translation := CameraPos;

  Rockets.Update(SecondsPassed);
  Explosions.Update(SecondsPassed);
  Enemies.Update(SecondsPassed);

  VisibleArea := Box3d(
    Vector3(Player.Position.X - SceneManager.Camera.Orthographic.EffectiveWidth / 2,
      0, -SceneManager.ProjectionSpan),
    Vector3(Player.Position.X + SceneManager.Camera.Orthographic.EffectiveWidth / 2,
      SceneManager.Camera.Orthographic.EffectiveHeight, SceneManager.ProjectionSpan)
  );

  if not TutorialDone then
  begin
    MessageOk(Window,
      'Keys:' + NL +
      '- left / right to run' + NL +
      '- up to jump' + NL +
      '- space to shoot (OK to keep it pressed)', hpLeft);
    TutorialDone := true;
  end;
end;

procedure WindowPress(Container: TUIContainer; const Event: TInputPressRelease);
const
  JumpDuration = 0.25;
begin
  if Event.IsKey(keyF2) then
    Map.Exists := not Map.Exists;
  if Event.IsKey(keyF3) then
    DebugNavigation.Exists := not DebugNavigation.Exists;
  if Event.IsKey(keyF5) then
    Window.SaveScreen(FileNameAutoInc(ApplicationName + '_screen_%d.png')) else
  if Event.IsKey(keyArrowUp) and
     (Player.Life > 0) and
     (Player.Jumping = 0) then
  begin
    Player.Jumping := JumpDuration;
  end;
end;

function MyGetApplicationName: string;
begin
  Result := 'in-space-everyone-is-screaming';
end;

initialization
  { This sets SysUtils.ApplicationName.
    It is useful to make sure it is correct (as early as possible)
    as our log routines use it. }
  OnGetApplicationName := @MyGetApplicationName;

  InitializeLog;

  { initialize Application callbacks }
  Application.OnInitialize := @ApplicationInitialize;

  { create Window and initialize Window callbacks }
  Window := TCastleWindow.Create(Application);
  Application.MainWindow := Window;
  Window.Container.UIScaling := usEncloseReferenceSize;
  Window.Container.UIReferenceWidth := 1024;
  Window.Container.UIReferenceHeight := 768;
  Window.OnRender := @WindowRender;
  Window.OnUpdate := @WindowUpdate;
  Window.OnPress := @WindowPress;
end.
